import PropTypes from 'prop-types';
import SectionComponent from './SectionComponent.js';
import Loader from 'react-loader-spinner';

export default function StorageField({ isLoading, age, name }) {
  return (
    <SectionComponent icon="/storage_icon.svg" title="Storage">
      <div className="relative">
        <div className="flex font-bold text-sm mb-4">
          <label className="text-gray-500 mr-3">Current Name:</label>
          <div className="text-blue-600 capitalize">{name}</div>
        </div>
        <div className="flex font-bold text-sm">
          <label className="text-gray-500 mr-3">Current Age:</label>
          <div className="text-blue-600 capitalize">{age}</div>
        </div>
        {isLoading && (
          <div className="absolute top-0 bottom-0 left-0 right-0 flex justify-center items-center flex-col bg-opacity-50 bg-white text-sm">
            <Loader
              type="TailSpin"
              color="#cacaca"
              height={35}
              width={35}
              className="m-auto"
            />
            Loading Storage...
          </div>
        )}
      </div>
    </SectionComponent>
  );
}

StorageField.propTypes = {
  isLoading: PropTypes.bool,
  age: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
};
