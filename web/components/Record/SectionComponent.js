import PropTypes from 'prop-types';
import React from 'react';
import Image from 'next/image';
export default function SectionComponent({ icon, title, children }) {
  return (
    <div className="bg-white border border-gray-200 rounded-md mt-10">
      <div className="border-b border-gray-200 p-5 flex items-center">
        <Image src={icon} width="30" height="23" alt={`${title} section icon`}/>
        <h3 className="font-bold ml-2"> {title}</h3>
      </div>
      <div className="px-5 py-7">{children}</div>
    </div>
  );
}

SectionComponent.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.component,
};
