import { useState } from 'react';

export function useContract(tezos, contractAddress) {
  const [contract, setContract] = useState(null);
  const [error, setError] = useState('');
  const [name, setName] = useState('');
  const [age, setAge] = useState(0);
  const [loading, setLoading] = useState(false);

  return {
    name,
    age,
    error,
    loading,
    contract,

    connect,
    reloadStorage,
  };

  function reloadStorage() {
    loadStorage(contract);
  }

  async function connect() {
    setLoading(true);
    try {
      const contractInstance = await tezos.wallet.at(contractAddress);
      setContract(contractInstance);
      await loadStorage(contractInstance);
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  }

  async function loadStorage(contract) {
    if (!contract) {
      return;
    }
    try {
      setLoading(true);
      const storage = await contract.storage();
      setAge(storage.age.toNumber());
      setName(storage.name);
    } catch (e) {
      setError(e.message);
    } finally {
      setLoading(false);
    }
  }
}
