import { useEffect } from 'react';
import { useRouter } from 'next/router';

import useBeacon from 'hooks/use-beacon';
import examplesData from 'data/examples.json';

import exampleComponents from 'components/examples';

export default function AddressPage() {
  const router = useRouter();
  const { address, id } = router.query;
  const { connect, pkh } = useBeacon();

  useEffect(() => {
    connect();
  }, [connect]);

  if (!(id in exampleComponents)) {
    return <div>Invalid example ID</div>;
  }

  const exampleData = examplesData.find((e) => e.id === id);

  if (!exampleData) {
    return <div>Example not found</div>;
  }

  const Component = exampleComponents[id];
  if (!pkh) {
    return null;
  }

  return <Component contractAddress={address} userAddress={pkh} />;
}
