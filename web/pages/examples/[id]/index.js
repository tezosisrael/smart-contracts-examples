import PropTypes from 'prop-types';

import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import ContractSelector from 'components/ContractSelector';

import examplesData from 'data/examples.json';

export default function ExamplePage({ id, contracts }) {
  const router = useRouter();
  const [contractAddress, setContractAddress] = useState('');

  useEffect(() => {
    if (!contracts || !contracts.length) {
      return;
    }
    setContractAddress(contracts[0].address);
  }, [contracts]);

  return (
    <ContractSelector
      onSubmit={handleConnect}
      value={contractAddress}
      onChange={setContractAddress}
      options={contracts}
    />
  );

  async function handleConnect() {
    router.push(`/examples/${id}/${contractAddress}`);
  }
}

ExamplePage.propTypes = {
  id: PropTypes.string.isRequired,
  contracts: PropTypes.arrayOf(
    PropTypes.shape({ address: PropTypes.string, lang: PropTypes.string })
  ),
};

export function getStaticPaths() {
  return {
    paths: examplesData.map((e) => ({ params: { id: e.id } })),
    fallback: false,
  };
}

export function getStaticProps({ params: { id } }) {
  const example = examplesData.find((e) => e.id === id);
  if (!example) {
    return { notFound: true };
  }

  return { props: { id, contracts: example.contracts } };
}
