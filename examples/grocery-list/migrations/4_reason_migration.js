const Grocery = artifacts.require('GroceryReason');
const { MichelsonMap } = require('@taquito/taquito');

module.exports = async (deployer, _network) => {
  const storage = MichelsonMap.fromLiteral({});
  deployer.deploy(Grocery, storage);
};
