type name = string ;
type amt = nat;
type storage = map(name, amt); 
type returnType = (list(operation), storage);

type action = 
| CreateItem ((name, amt))
| RemoveItem (name)
| ChangeAmount ((name, amt));

let createItem = ((name, amt, s) : (name, amt, storage)) : returnType => 
switch(Map.find_opt(name, s)){
  |Some(item) => ((failwith("ItemAlreadyExists") : returnType))
  |None => ([]: list(operation), Map.add(name, amt, s))
}
let removeItem = ((name,  s) : (name, storage)) : returnType => 
switch(Map.find_opt(name, s)){
  |Some(item) => ([]: list(operation), Map.remove(name, s))
  |None => ((failwith("NoSuchItem") : returnType)) 
}

let changeAmount = ((name, amt, s) : (name, amt, storage)) : returnType =>  
switch(Map.find_opt(name, s)){
  |Some(item) => ([]: list(operation), Map.update(name, (Some(amt)), s))
  |None => ((failwith("NoSuchItem") : returnType)) 
}

let main = ((action,s): (action, storage)) : returnType => {
    switch (action) {
      | CreateItem (n) => createItem(n[0], n[1], s) 
      | RemoveItem (n) =>  removeItem(n, s) 
      | ChangeAmount (n) =>  changeAmount(n[0], n[1], s) 
    };
};