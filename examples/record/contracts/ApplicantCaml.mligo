type action = 
    | SetName of string
    | SetAge of nat
    | SetAll of string * nat

type storage = {
    name : string;
    age : nat
}

type return = operation list * storage

let setName(name, store : string * storage) : return =
    ([] : operation list), {store with name = name}

let setAge(age, store : nat * storage) : return =
    ([] : operation list), {store with age = age}

let setAll(name, age, store : string * nat * storage) : return =
    ([] : operation list), {store with name = name; age = age}

let main (action, store : action * storage) : return =
    match action with
    | SetName v -> setName(v, store)
    | SetAge v -> setAge(v, store)
    | SetAll v -> setAll(v.0, v.1, store)
