# Record Contract Example

## A record that holds a name and an age (Like an application submission)

---

#### Source Code:

- **Caml:** [ApplicantCaml.mligo](https://gitlab.com/tezosisrael/smart-contracts-examples/-/blob/master/record/truffle/contracts/ApplicantCaml.mligo)
- **Pascal:** [ApplicantPascal.ligo]()
- **Reason:** [ApplicantReason.religo](https://gitlab.com/tezosisrael/smart-contracts-examples/-/blob/master/record/truffle/contracts/ApplicantReason.religo)

#### Contract Addresses:

- **Caml:** [KT1C7RUVaa6H6iUt6WYZE5fiywEEt1ZPj8pk](https://better-call.dev/florencenet/KT1C7RUVaa6H6iUt6WYZE5fiywEEt1ZPj8pk/operations)
- **Pascal:** [KT1VTPpRKyC5erME5UU3a17tajmUDTf5giAo](https://better-call.dev/florencenet/KT1VTPpRKyC5erME5UU3a17tajmUDTf5giAo/operations)
- **Reason:** [KT1HU6cWSUzqFFczZPHJcCTAL4KoSWiiaA5h](https://better-call.dev/florencenet/KT1HU6cWSUzqFFczZPHJcCTAL4KoSWiiaA5h/operations)

---

### API:

#### Entry points:

1. `SetName(string)`
2. `SetAge(nat)`
3. `SetAll(string, nat)`

#### Storage:

```ocaml
record {
  name : string;
  age : nat
}
```

---

#### Contributors:

- reason: [@adamshinder](https://gitlab.com/adamshinder)
- caml: [@ahla85](https://gitlab.com/ahla85)
- pascal: [@ahla85](https://gitlab.com/ahla85)
- dapp: [@adamshinder](https://gitlab.com/adamshinder) [Tezos Israel Dev-Team](https://gitlab.com/tezosisrael)
