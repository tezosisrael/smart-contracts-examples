const { pkh } = require('../faucet.json');
const auction = artifacts.require('AuctionCaml');

module.exports = async (deployer, _network) => {
  const storage = {
    current_bidder: null,
    end_time: '0',
    current_price: 0,
    owner: pkh,
    is_first_bid: true,
    starting_price: 0,
  };

  try {
    deployer.deploy(auction, storage);
  } catch (e) {
    e.error.message;
  }
};
