//change all occurances of 'Grocery' to fit contract names

const assert = require('assert');

['Caml' /*, "Pascal", "Reason"*/].forEach((lang) => {
  const contractName = 'Auction' + lang;
  const Auction = artifacts.require(contractName);

  contract(contractName, async function () {
    let instance = null;

    before(async () => {
      instance = await Auction.deployed();
    });

    // First test - check if the storage was deployed correctly

    it('Should check the initial storage', async function () {
      const storage = await instance.storage();
      //   console.log(storage);
      //   assert.strictEqual(storage.bid_list.size, 0);
    });
  });
});
