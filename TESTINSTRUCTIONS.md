# Test Instructions

## Below are instructions for testing and deploying our contracts

### Requirements:

- Install and run docker.
- Clone the contract you want to test from "Source Code" button.
- for use of sandbox mode - Run sandbox using `yarn start-sandbox`.

### Running contract tests

- for running the tests in sandbox mode - use `truffle test` from the example's main folder
- for running the tests on the testnet - use `truffle test --network testnet`

### Deploying the contract

- for deploying onto the sandbox - use `truffle migrate` from the example's main folder
- for deploying onto the testnet - use `truffle migrate --network testnet`
